package ge.edu.btu.learnbank.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.databinding.AccountItemBinding
import ge.edu.btu.learnbank.ui.models.AccountsModel


class AccountsRecyclerAdapter(
    private val items: ArrayList<AccountsModel>
) :
    RecyclerView.Adapter<AccountsRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: AccountItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.account_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.onBind()

    override fun getItemCount() = items.size

    inner class ViewHolder(private val binding: AccountItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.accounts = items[adapterPosition]
        }
    }
}
