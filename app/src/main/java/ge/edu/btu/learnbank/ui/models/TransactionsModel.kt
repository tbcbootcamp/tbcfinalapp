package ge.edu.btu.learnbank.ui.models

import com.google.gson.annotations.SerializedName

class TransactionsModel(
    val id: Int = 0,
    @SerializedName("external_transaction_id")
    val externalTransactionId: Long = 0,
    val transaction_date: String = "",
    val description: String = "",
    val status: Int = 0,
    val amount: Long = 0,
    val currency: String = "",
    val transaction_type: String = "",
    val client_account_number: String = "",
    val transaction_category_id: Int = 0,
    val created_at: String = "",
    val updated_at: String = ""
)