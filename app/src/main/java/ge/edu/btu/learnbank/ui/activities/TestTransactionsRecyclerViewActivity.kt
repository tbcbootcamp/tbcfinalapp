package ge.edu.btu.learnbank.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import ge.edu.btu.learnbank.App
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.data.ApiMethods
import ge.edu.btu.learnbank.data.DataLoader
import ge.edu.btu.learnbank.data.FutureCallBack
import ge.edu.btu.learnbank.ui.models.TransactionsModel
import kotlinx.android.synthetic.main.activity_test_transactions_recycler_view.*

class TestTransactionsRecyclerViewActivity : AppCompatActivity() {

    private val items = ArrayList<TransactionsModel>()
    private lateinit var adapter: TestTransactionsRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_transactions_recycler_view)
        init()
    }


    private fun init() {
        loadData()
        transactionsRecyclerView.layoutManager = LinearLayoutManager(App.instance.getContext())
        adapter = TestTransactionsRecyclerViewAdapter(items)
        transactionsRecyclerView.adapter = adapter

        backButton.setOnClickListener {
            finish()
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        }
    }

    private fun loadData() {
        DataLoader.getRequest(
            null,
            ApiMethods.TRANSACTIONS,
            object : FutureCallBack<String> {
                override fun onSuccess(result: String) {
                    d("transactions", result)
                    val data: Array<TransactionsModel> = Gson().fromJson(
                        result,
                        Array<TransactionsModel>::class.java
                    )
                    items.addAll(data)
                    adapter.notifyDataSetChanged()

                }

                override fun onFailure(title: String, errorMessage: String) {
                    d("transactions", errorMessage)
                }
            }
        )
    }
}