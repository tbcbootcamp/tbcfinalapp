package ge.edu.btu.learnbank.ui.models

class UserSignUpModel(
    val user: UserData? = null,
    val token: String = ""
)

class UserData(
    val name: String = "",
    val lastname: String = "",
    val email: String = "",
    val birth_date: String = "",
    val phone_number: String = "",
    val address: String = "",
    val personal_number: String = "",
    val status: Int = 0,
    val image_key: Int = 0,
    val updated_at: String = "",
    val created_at: String = "",
    val id: Int = 0
)