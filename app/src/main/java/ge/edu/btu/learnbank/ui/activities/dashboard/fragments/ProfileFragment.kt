package ge.edu.btu.learnbank.ui.activities.dashboard.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.data.ApiMethods
import ge.edu.btu.learnbank.data.DataLoader
import ge.edu.btu.learnbank.data.FutureCallBack
import ge.edu.btu.learnbank.ui.activities.TestTransactionsRecyclerViewActivity
import ge.edu.btu.learnbank.ui.activities.auth.LoginActivity
import ge.edu.btu.learnbank.ui.models.UserProfileModel
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*


class ProfileFragment : Fragment() {


    private lateinit var itemView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_profile, container, false)
        init()
        return itemView
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadData()
    }


    private fun loadData() {
        DataLoader.getRequest(
            null,
            ApiMethods.PROFILE,
            object : FutureCallBack<String> {
                override fun onSuccess(result: String) {
                    val data = Gson().fromJson(result, UserProfileModel::class.java)
                    profileNameTextView.text = "${data.user!!.name} ${data.user.lastname}"
                }

                override fun onFailure(title: String, errorMessage: String) {

                }
            }
        )
    }

    private fun init() {
        itemView.logOutButton.setOnClickListener {
            val intent = Intent(itemView.context, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        itemView.menuItemTransactions.setOnClickListener {
            val intent = Intent(itemView.context, TestTransactionsRecyclerViewActivity::class.java)
            startActivity(intent)
        }
    }
}