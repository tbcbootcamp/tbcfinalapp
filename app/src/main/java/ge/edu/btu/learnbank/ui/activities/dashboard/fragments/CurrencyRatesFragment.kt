package ge.edu.btu.learnbank.ui.activities.dashboard.fragments

import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import ge.edu.btu.learnbank.App
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.data.ApiMethods
import ge.edu.btu.learnbank.data.DataLoader
import ge.edu.btu.learnbank.data.FutureCallBack
import ge.edu.btu.learnbank.ui.adapters.CurrencyAdapter
import ge.edu.btu.learnbank.ui.models.CurrencyResponseModel
import kotlinx.android.synthetic.main.fragment_currency_rates.*

class CurrencyRatesFragment : Fragment() {

    private lateinit var itemView: View
    private val items = ArrayList<CurrencyResponseModel>()
    private lateinit var adapter: CurrencyAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecyclerView()


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_currency_rates, container, false)
        return itemView
    }


    private fun initRecyclerView() {
        loadData()
        currencyRecyclerView.layoutManager = LinearLayoutManager(App.instance.getContext())
        adapter = CurrencyAdapter(items)
        currencyRecyclerView.adapter = adapter


        currencyRecyclerViewRefreshLayout.setOnRefreshListener {
            items.clear()
            loadData()
            currencyRecyclerViewRefreshLayout.isRefreshing = false
        }

    }

    private fun loadData() {
        DataLoader.getRequest(
            null,
            ApiMethods.RATES,
            object : FutureCallBack<String> {
                override fun onSuccess(result: String) {
                    d("currencyResponse", result)
                    val data: Array<CurrencyResponseModel> =
                        Gson().fromJson(result, Array<CurrencyResponseModel>::class.java)
                    items.addAll(data)
                    adapter.notifyDataSetChanged()
                }

                override fun onFailure(title: String, errorMessage: String) {
                    d("currencyResponse", title)
                }
            }
        )
    }
}