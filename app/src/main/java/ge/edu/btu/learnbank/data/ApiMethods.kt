package ge.edu.btu.learnbank.data

class ApiMethods {
    companion object {
        const val LOGIN = "login"
        const val REGISTER = "register"
        const val CARDS = "cards"
        const val PROFILE = "profile"
        const val ACCOUNTS = "accounts"
        const val RATES = "rates"
        const val TRANSACTIONS = "transactions"
        const val TRANSFER = "transfer"
        const val CHECK = "checkToken"
    }
}