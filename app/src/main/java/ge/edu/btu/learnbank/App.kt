package ge.edu.btu.learnbank

import android.app.Application
import android.content.Context

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
    }

    fun getContext(): Context = context!!


    private object Holder {
        val INSTANCE: App = App()
    }

    companion object {
        val instance: App by lazy {
            Holder.INSTANCE
        }
        internal var context: Context? = null
    }

}