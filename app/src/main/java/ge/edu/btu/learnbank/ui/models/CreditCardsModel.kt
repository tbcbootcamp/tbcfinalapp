package ge.edu.btu.learnbank.ui.models

import com.google.gson.annotations.SerializedName



class   CreditCardsModel(
    val id: Int = 0,
    val iban: String = "",
    val currency: String = "",
    val status: Int = 0,
    val type: String = "",
    @SerializedName("available_balance")
    val availableBalance: Long = 0,
    val card_class: String = "",
    val name: String = "",
    val last_numbers: Int = 0,
    val expire_date: String = "",
    val is_active: Int = 0,
    val imageSource: String = "https://online.cloud.com.ge/resources/cards/v1/7.png"
)