package ge.edu.btu.learnbank.ui.activities

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ge.edu.btu.learnbank.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUserData()
    }


    private fun setUserData() {
        val sharePreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        userAccessTokenTextView.text = sharePreferences.getString("access_token", "")
    }
}