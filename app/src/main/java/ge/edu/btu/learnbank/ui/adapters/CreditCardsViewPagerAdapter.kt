package ge.edu.btu.learnbank.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.databinding.CardItemBinding
import ge.edu.btu.learnbank.databinding.CardItemViewPagerBinding
import ge.edu.btu.learnbank.ui.models.CreditCardsModel

class CreditCardsViewPagerAdapter(
    private val items: ArrayList<CreditCardsModel>
) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount() = items.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding = CardItemViewPagerBinding.inflate(
            LayoutInflater.from(container.context),
            container,
            false
        )
        binding.card = items[position]
        container.addView(binding.root)
        return binding.root
    }

}