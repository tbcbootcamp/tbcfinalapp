package ge.edu.btu.learnbank.utils

import android.app.Activity
import com.tapadoo.alerter.Alerter
import ge.edu.btu.learnbank.R

fun showErrorMessage(activity: Activity, title: String, message: String) {
    Alerter.create(activity)
        .setText(message)
        .setTitle(title)
        .setBackgroundColor(R.color.colorPrimaryRed)
        .show()
}

fun showSuccessMessage(activity: Activity, title: String, message: String) {
    Alerter.create(activity)
        .setText(message)
        .setTitle(title)
        .setBackgroundColor(R.color.successMessageAlertColor)
        .show()
}