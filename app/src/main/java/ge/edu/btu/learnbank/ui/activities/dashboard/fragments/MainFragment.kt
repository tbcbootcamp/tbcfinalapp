package ge.edu.btu.learnbank.ui.activities.dashboard.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.data.ApiMethods
import ge.edu.btu.learnbank.data.DataLoader
import ge.edu.btu.learnbank.data.FutureCallBack
import ge.edu.btu.learnbank.ui.adapters.CreditCardsViewPagerAdapter
import ge.edu.btu.learnbank.ui.models.CreditCardsModel
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : Fragment() {

    private val items = ArrayList<CreditCardsModel>()
    private lateinit var adapter: CreditCardsViewPagerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init()
    }


    private fun init() {
        creditCardsViewPager.pageMargin = 50
        loadCards()
        adapter = CreditCardsViewPagerAdapter(items)
        creditCardsViewPager.adapter = adapter
    }


    private fun loadCards() {
        DataLoader.getRequest(
            null,
            ApiMethods.CARDS,
            object : FutureCallBack<String> {
                override fun onSuccess(result: String) {
                    val data = Gson().fromJson(result, Array<CreditCardsModel>::class.java)
                    items.addAll(data)
                    adapter.notifyDataSetChanged()
                }

                override fun onFailure(title: String, errorMessage: String) {

                }
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }
}