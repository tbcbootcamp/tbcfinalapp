package ge.edu.btu.learnbank.ui.activities.dashboard

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.ui.activities.dashboard.fragments.AccountsFragment
import ge.edu.btu.learnbank.ui.activities.dashboard.fragments.CurrencyRatesFragment
import ge.edu.btu.learnbank.ui.activities.dashboard.fragments.MainFragment
import ge.edu.btu.learnbank.ui.activities.dashboard.fragments.ProfileFragment
import ge.edu.btu.learnbank.ui.adapters.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        init()
    }


    private fun init() {
        val fragments = mutableListOf<Fragment>()
        fragments.add(MainFragment())
        fragments.add(AccountsFragment())
        fragments.add(CurrencyRatesFragment())
        fragments.add(ProfileFragment())
        viewPager.offscreenPageLimit = 4
        viewPager.adapter = ViewPagerAdapter(supportFragmentManager, fragments)
        viewPagerIssues()
    }


    private fun viewPagerIssues() {
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                nav_view.menu.getItem(position).isChecked = true

            }
        })

        nav_view.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_main -> viewPager.currentItem = 0
                R.id.navigation_accounts -> viewPager.currentItem = 1
                R.id.navigation_currency_rates -> viewPager.currentItem = 2
                R.id.navigation_profile -> viewPager.currentItem = 3
            }
            true
        }
    }
}