package ge.edu.btu.learnbank.ui.activities.auth

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import ge.edu.btu.learnbank.App
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.data.ApiMethods
import ge.edu.btu.learnbank.data.DataLoader
import ge.edu.btu.learnbank.data.FutureCallBack
import ge.edu.btu.learnbank.ui.activities.dashboard.DashboardActivity
import ge.edu.btu.learnbank.ui.models.UserLoginModel
import ge.edu.btu.learnbank.utils.UserPreference
import ge.edu.btu.learnbank.utils.showErrorMessage
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.MediaType
import okhttp3.RequestBody

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }

    private fun init() {
        userLoginButton.setOnClickListener {

            if (userEmailAddressEditText.text.isNotEmpty() && userPasswordEditText.text.isNotEmpty()) {

                val parameters = HashMap<String, @JvmSuppressWildcards RequestBody>()
                parameters["email"] = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    userEmailAddressEditText.text.toString()
                )
                parameters["password"] = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    userPasswordEditText.text.toString()
                )

                DataLoader.postRequest(
                    userAuthProgressBar,
                    ApiMethods.LOGIN,
                    parameters,
                    object :
                        FutureCallBack<String> {
                        override fun onSuccess(result: String) {

                            val userAuthModel = Gson().fromJson(result, UserLoginModel::class.java)

                            if (userAuthModel.token.isNotEmpty()) {
                                UserPreference.saveString(UserPreference.TOKEN, userAuthModel.token)
                                val intent =
                                    Intent(this@LoginActivity, DashboardActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                startActivity(intent)
                                overridePendingTransition(
                                    android.R.anim.fade_in,
                                    android.R.anim.fade_out
                                )
                            } else {
                                showErrorMessage(
                                    this@LoginActivity,
                                    App.instance.getContext().getString(R.string.error),
                                    "Error occurred"
                                )
                            }
                        }

                        override fun onFailure(title: String, errorMessage: String) {
                            showErrorMessage(
                                this@LoginActivity,
                                App.instance.getContext().getString(R.string.error),
                                errorMessage
                            )
                        }
                    }
                )
            } else {
                showErrorMessage(
                    this@LoginActivity,
                    App.instance.getContext().getString(R.string.error),
                    App.instance.getContext().getString(R.string.enter_email_and_password)
                )
            }
        }
    }
}