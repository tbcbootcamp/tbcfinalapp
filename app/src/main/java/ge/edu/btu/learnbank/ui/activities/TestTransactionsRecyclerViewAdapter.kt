package ge.edu.btu.learnbank.ui.activities

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.databinding.TransactionItemBinding
import ge.edu.btu.learnbank.ui.models.TransactionsModel

class TestTransactionsRecyclerViewAdapter(
    private val items: ArrayList<TransactionsModel>
) : RecyclerView.Adapter<TestTransactionsRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: TransactionItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.transaction_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.onBind()

    inner class ViewHolder(private val binding: TransactionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.transaction = items[adapterPosition]
        }
    }


}
