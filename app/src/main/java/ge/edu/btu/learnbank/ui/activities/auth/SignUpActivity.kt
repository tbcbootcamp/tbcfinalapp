package ge.edu.btu.learnbank.ui.activities.auth

import android.os.Bundle
import android.util.Patterns
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import ge.edu.btu.learnbank.App
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.data.ApiMethods
import ge.edu.btu.learnbank.data.DataLoader
import ge.edu.btu.learnbank.data.FutureCallBack
import ge.edu.btu.learnbank.ui.models.UserSignUpModel
import ge.edu.btu.learnbank.utils.showErrorMessage
import ge.edu.btu.learnbank.utils.showSuccessMessage
import kotlinx.android.synthetic.main.activity_sign_up.*
import okhttp3.MediaType
import okhttp3.RequestBody


class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init() {
        userSignUpButton.setOnClickListener {
            if (checkEmptyFields() && checkPasswords() && isEmailValid(signUpEmailET.text.toString())) {

                val parameters = HashMap<String, @JvmSuppressWildcards RequestBody>()

                parameters["email"] = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    signUpEmailET.text.toString()
                )
                parameters["name"] = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    signUpNameET.text.toString()
                )
                parameters["lastname"] = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    signUpSurnameET.text.toString()
                )
                parameters["birth_date"] = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    "2001-02-08"
                )
                parameters["phone_number"] = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    phoneNumberET.text.toString()
                )
                parameters["address"] = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    "test address"
                )
                parameters["password"] = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    signUpPasswordET.text.toString()
                )
                parameters["password_confirmation"] = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    confirmPasswordET.text.toString()
                )
                parameters["personal_number"] = RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    "1212321312"
                )

                DataLoader.postRequest(
                    userAuthProgressBar,
                    ApiMethods.REGISTER,
                    parameters,
                    object :
                        FutureCallBack<String> {
                        override fun onSuccess(result: String) {

                            val userAuthModel = Gson().fromJson(result, UserSignUpModel::class.java)
                            if (userAuthModel.token.isNotEmpty()) {
                                showSuccessMessage(
                                    this@SignUpActivity,
                                    "success",
                                    "Registration completed successfully"
                                )

                            } else {
                                showErrorMessage(
                                    this@SignUpActivity,
                                    App.instance.getContext().getString(R.string.error),
                                    "Error occurred"
                                )
                            }
                        }

                        override fun onFailure(title: String, errorMessage: String) {
                            showErrorMessage(
                                this@SignUpActivity,
                                App.instance.getContext().getString(R.string.error),
                                errorMessage
                            )
                        }
                    }
                )
            } else {
                fieldsChecker()
            }
        }
    }

    private fun checkEmptyFields(): Boolean {
        return signUpEmailET.text.isNotEmpty() &&
                signUpNameET.text.isNotEmpty() &&
                signUpSurnameET.text.isNotEmpty() &&
                phoneNumberET.text.isNotEmpty() &&
                signUpPasswordET.text.isNotEmpty() &&
                confirmPasswordET.text.isNotEmpty()
    }


    private fun checkPasswords(): Boolean {
        return signUpPasswordET.text.toString() == confirmPasswordET.text.toString()
    }

    private fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }


    private fun fieldsChecker() {
        if (!checkEmptyFields()) {
            showErrorMessage(
                this@SignUpActivity,
                App.instance.getContext().getString(R.string.error),
                getString(R.string.fill_all_fields)
            )
        } else if (!checkPasswords()) {
            showErrorMessage(
                this@SignUpActivity,
                App.instance.getContext().getString(R.string.error),
                getString(R.string.passwords_dont_match)
            )
        } else if (!isEmailValid(signUpEmailET.text.toString())) {
            showErrorMessage(
                this@SignUpActivity,
                App.instance.getContext().getString(R.string.error),
                "Enter correct e-mail"
            )
        }
    }


}