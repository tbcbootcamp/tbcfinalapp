package ge.edu.btu.learnbank.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.databinding.CurrencyItemBinding
import ge.edu.btu.learnbank.ui.models.CurrencyResponseModel

class CurrencyAdapter(private val items: ArrayList<CurrencyResponseModel>) :
    RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyAdapter.ViewHolder {
        val binding: CurrencyItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.currency_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(private val binding: CurrencyItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.currency = items[adapterPosition]
        }
    }
}