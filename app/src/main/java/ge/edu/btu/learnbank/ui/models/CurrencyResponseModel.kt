package ge.edu.btu.learnbank.ui.models

data class CurrencyResponseModel(
    val currency: String = "",
    val currencyName: String = "",
    val buyRate: Double = 0.0,
    val sellRate: Double = 0.0,
    val nbgRate: Double = 0.0,
    val currencyCountryFlag: String = ""
)