package ge.edu.btu.learnbank.data

interface FutureCallBack<T> {
    fun onSuccess(result: String) {}
    fun onFailure(title: String, errorMessage: String) {}
}