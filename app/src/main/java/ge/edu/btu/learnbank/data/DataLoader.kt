package ge.edu.btu.learnbank.data

import android.content.Intent
import android.util.Log.d
import android.view.View
import ge.edu.btu.learnbank.App
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.ui.activities.auth.AuthActivity
import ge.edu.btu.learnbank.utils.UserPreference
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


object DataLoader {

    private const val HTTP_200_OK = 200
    private const val HTTP_201_CREATED = 201
    private const val HTTP_400_BAD_REQUEST = 400
    private const val HTTP_401_UNAUTHORIZED = 401
    private const val HTTP_404_NOT_FOUND = 404
    private const val HTTP_500_INTERNAL_SERVER_ERROR = 500
    private const val HTTP_204_NO_CONTENT = 204


    private val httpClient = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.MINUTES)
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(15, TimeUnit.SECONDS)
        .addInterceptor { chain ->
            val token = UserPreference.getString(UserPreference.TOKEN)!!
            val request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
            if (token.isNotEmpty())
                request.addHeader("Authorization", "Bearer $token")
            chain.proceed(request.build())
        }

    private var retrofit = Retrofit.Builder()
        .baseUrl(RetrofitApi.BASE_URL)
        .addConverterFactory(ScalarsConverterFactory.create())
        .client(httpClient.build())
        .build()

    private var api = retrofit.create(
        RetrofitApi::class.java
    )

    fun postRequest(
        loadingView: View? = null,
        path: String,
        requestBody: HashMap<String, @JvmSuppressWildcards RequestBody>,
        callback: FutureCallBack<String>
    ) {
        if (loadingView != null)
            Tools.viewVisibility(loadingView)
        if (Tools.isInternetOn()) {
            val call = api.postRequest(path, requestBody)

            val parameters = HashMap<String, String>()
            for (item in requestBody) {
                parameters[item.key] = item.value.toString()
            }
            call.enqueue(
                baseCallback(
                    loadingView,
                    parameters,
                    callback
                )
            )
        } else
            callback.onFailure(
                App.instance.getContext().getString(
                    R.string.incorrect_request
                ),
                App.instance.getContext().getString(
                    R.string.no_internet
                )
            )
    }


    fun getRequest(
        loadingView: View? = null,
        path: String,
        callback: FutureCallBack<String>
    ) {

        if (loadingView != null)
            Tools.viewVisibility(loadingView)
        if (Tools.isInternetOn()) {
            val call = api.getRequest(path)
            call.enqueue(
                baseCallback(
                    loadingView,
                    null,
                    callback
                )
            )
        } else
            callback.onFailure(
                App.instance.getContext().getString(
                    R.string.incorrect_request
                ),
                App.instance.getContext().getString(
                    R.string.no_internet
                )
            )
    }

    private fun baseCallback(
        loadingView: View? = null,
        parameters: MutableMap<String, String>?,
        callback: FutureCallBack<String>
    ): Callback<String> {
        return object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                d("serverResponse", " success " + t.message)
                callback.onFailure(
                    App.instance.getContext().getString(
                        R.string.incorrect_request
                    ),
                    App.instance.getContext().getString(
                        R.string.an_error_occurred_please_try_again
                    )
                )
                if (loadingView != null)
                    Tools.viewVisibility(
                        loadingView
                    )
            }

            override fun onResponse(call: Call<String>, response: retrofit2.Response<String>) {
                handleResponseCode(
                    loadingView,
                    response,
                    callback
                )
            }
        }
    }

    private fun handleResponseCode(
        loadingView: View? = null,
        response: retrofit2.Response<String>,
        callback: FutureCallBack<String>
    ) {
        if (loadingView != null)
            Tools.viewVisibility(loadingView)
        if (response.code() == HTTP_200_OK || response.code() == HTTP_201_CREATED)
            try {
                callback.onSuccess(response.body()!!)
            } catch (e: JSONException) {
                callback.onFailure(
                    App.instance.getContext().resources.getString(
                        R.string.incorrect_request
                    ),
                    App.instance.getContext().getString(
                        R.string.an_error_occurred_please_try_again
                    )
                )
            }
        else if (response.code() == HTTP_400_BAD_REQUEST)
            handleError(
                response.errorBody()!!.string(),
                callback
            )
        else if (response.code() == HTTP_401_UNAUTHORIZED)
            handleError(
                response.errorBody()!!.string(),
                callback
            )
        else if (response.code() == HTTP_404_NOT_FOUND)
            handleError(
                response.errorBody()!!.string(),
                callback
            )
        else if (response.code() == HTTP_500_INTERNAL_SERVER_ERROR)
            handleError(
                response.errorBody()!!.string(),
                callback
            )
        else if (response.code() == HTTP_204_NO_CONTENT)
            handleError("", callback)
        else {
            d("serverResponse", " ${response.errorBody()!!.string()}")
            callback.onFailure(
                App.instance.getContext().resources.getString(
                    R.string.incorrect_request
                ),
                App.instance.getContext().getString(
                    R.string.an_error_occurred_please_try_again
                )
            )
        }
    }

    private fun handleError(
        message: String,
        callback: FutureCallBack<String>
    ) {
        try {
            val jsonObject = JSONObject(message)
            when {
                jsonObject.has("error") -> callback.onFailure(
                    App.instance.getContext().resources.getString(
                        R.string.incorrect_request
                    ), jsonObject.getString("error")
                )
                else -> callback.onFailure(
                    App.instance.getContext().getString(
                        R.string.incorrect_request
                    ),
                    App.instance.getContext().getString(
                        R.string.an_error_occurred_please_try_again
                    )
                )
            }
        } catch (e: JSONException) {
            callback.onFailure(
                App.instance.getContext().getString(
                    R.string.incorrect_request
                ),
                App.instance.getContext().getString(
                    R.string.an_error_occurred_please_try_again
                )
            )
        }
    }

    interface RetrofitApi {

        companion object {
            const val BASE_URL = "https://online.cloud.com.ge/api/v1/"
        }

        @Multipart
        @POST("{path}")
        fun postRequest(
            @Path("path") path: String,
            @PartMap parameters: Map<String, @JvmSuppressWildcards RequestBody>
        ): Call<String>

        @GET("{path}")
        fun getRequest(
            @Path("path") path: String
        ): Call<String>
    }
}


fun redirectToAuthActivity(): Intent {
    return Intent(App.instance.getContext(), AuthActivity::class.java)
}