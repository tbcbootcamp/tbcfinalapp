package ge.edu.btu.learnbank.ui.components

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import ge.edu.btu.learnbank.App
import ge.edu.btu.learnbank.R

object DataBindingComponents {
    @JvmStatic
    @BindingAdapter("loadImage")
    fun loadImage(view: ImageView, imageUrl: String) {
        Glide
            .with(App.instance.getContext())
            .load(imageUrl)
            .placeholder(R.mipmap.ic_launcher)
            .centerCrop()
            .into(view)
    }
}