package ge.edu.btu.learnbank.data

import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import ge.edu.btu.learnbank.App


class Tools {

    companion object {

        fun isInternetOn(): Boolean {
            val cm = App.instance.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
            val activeNetwork = cm?.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnected
        }

        fun viewVisibility(view: View) {
            if (view.visibility == View.VISIBLE)
                view.visibility = View.INVISIBLE
            else
                view.visibility = View.VISIBLE
        }
    }
}   