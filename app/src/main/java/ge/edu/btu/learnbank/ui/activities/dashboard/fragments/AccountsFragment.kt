package ge.edu.btu.learnbank.ui.activities.dashboard.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import ge.edu.btu.learnbank.App
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.data.ApiMethods
import ge.edu.btu.learnbank.data.DataLoader
import ge.edu.btu.learnbank.data.FutureCallBack
import ge.edu.btu.learnbank.ui.activities.transfer.TransfersActivity
import ge.edu.btu.learnbank.ui.adapters.AccountsRecyclerAdapter
import ge.edu.btu.learnbank.ui.models.AccountsModel
import kotlinx.android.synthetic.main.fragment_accounts.*


class AccountsFragment : Fragment() {

    private val items = ArrayList<AccountsModel>()
    private lateinit var adapter: AccountsRecyclerAdapter


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        transferButton.setOnClickListener {
            val intent = Intent(App.instance.getContext(), TransfersActivity::class.java)
            startActivity(intent)
        }
        setUp()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_accounts, container, false)
    }


    private fun setUp() {

//        items.add(CreditCardsModel(1, "GE021312311", "GEL", 1, 1, 1300))

        loadCards()

        creditCardsRecyclerView.layoutManager = LinearLayoutManager(App.instance.getContext())
        adapter = AccountsRecyclerAdapter(items)
        creditCardsRecyclerView.adapter = adapter
    }


    private fun loadCards() {
        DataLoader.getRequest(
            null,
            ApiMethods.ACCOUNTS,
            object : FutureCallBack<String> {
                override fun onSuccess(result: String) {
                    val data = Gson().fromJson(result, Array<AccountsModel>::class.java)
                    items.addAll(data)
                    adapter.notifyDataSetChanged()
                }

                override fun onFailure(title: String, errorMessage: String) {

                }
            }
        )
    }


}