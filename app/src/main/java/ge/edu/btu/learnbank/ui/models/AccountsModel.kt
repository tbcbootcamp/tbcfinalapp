package ge.edu.btu.learnbank.ui.models

import com.google.gson.annotations.SerializedName

class AccountsModel (
    val id: Int = 0,
    @SerializedName("iban")
    val accountNumber: String = "",
    val type: String = "",
    val canBeClosed: Int = 0,
    val isPrimary: Int = 0,
    val isHiddenAccount: Int = 0,
    val friendlyName: String = "",
    val primaryCurrency: String = "",
    @SerializedName("available_balance")
    val availableBalance: Long = 0,
    val status: Int = 0,
    val openDate: String = ""

)