package ge.edu.btu.learnbank.ui.activities.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import ge.edu.btu.learnbank.App
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.data.ApiMethods
import ge.edu.btu.learnbank.data.DataLoader
import ge.edu.btu.learnbank.data.FutureCallBack
import ge.edu.btu.learnbank.data.Tools
import ge.edu.btu.learnbank.ui.activities.auth.AuthActivity
import ge.edu.btu.learnbank.ui.activities.dashboard.DashboardActivity
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : AppCompatActivity() {

    private val handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    private val runnable = Runnable {

        splashScreenLogoImageView.startAnimation(
            AnimationUtils.loadAnimation(
                this, R.anim.splash_screen_animation
            )
        )
        handler.postDelayed({
            startActivityChange()
        }, 1000)
    }


    private fun startActivityChange() {

        Tools.viewVisibility(splashScreenLogoImageView)

        DataLoader.getRequest(
            null,
            ApiMethods.CHECK,
            object : FutureCallBack<String> {
                override fun onSuccess(result: String) {
                    val intent = Intent(App.instance.getContext(), DashboardActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    startActivity(intent)
                }

                override fun onFailure(title: String, errorMessage: String) {
                    val intent = Intent(App.instance.getContext(), AuthActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    startActivity(intent)
                }
            }
        )

    }


    override fun onStart() {
        super.onStart()
        handler.postDelayed(runnable, 2000)
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
    }

}