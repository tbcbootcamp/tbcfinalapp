package ge.edu.btu.learnbank.ui.models

class UserProfileModel(
    val user: user? = null
)
class user (
    val id: Int = 0,
    val name: String = "",
    val lastname: String = "",
    val email: String = "",
    val email_verified_at: String? = "",
    val birth_date: String = "",
    val personal_number: String = "",
    val phone_number: String = "",
    val address: String = "",
    val status: Int = 0,
    val image_key: Int = 0,
    val created_at: String = "",
    val updated_at: String = ""
)