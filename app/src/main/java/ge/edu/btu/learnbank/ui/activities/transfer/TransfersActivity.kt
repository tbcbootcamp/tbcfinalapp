package ge.edu.btu.learnbank.ui.activities.transfer

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import ge.edu.btu.learnbank.App
import ge.edu.btu.learnbank.R
import ge.edu.btu.learnbank.data.ApiMethods
import ge.edu.btu.learnbank.data.DataLoader
import ge.edu.btu.learnbank.data.FutureCallBack
import ge.edu.btu.learnbank.ui.models.TransferModel
import ge.edu.btu.learnbank.utils.showErrorMessage
import ge.edu.btu.learnbank.utils.showSuccessMessage
import kotlinx.android.synthetic.main.activity_transfers.*
import okhttp3.MediaType
import okhttp3.RequestBody

class TransfersActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfers)
        init()
    }

    private fun init() {
        transferButton.setOnClickListener {

            val parameters = HashMap<String, @JvmSuppressWildcards RequestBody>()

            parameters["from_iban"] = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                senderIbanEditText.text.toString()
            )

            parameters["to_iban"] = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                receiverIbanEditText.text.toString()
            )

            parameters["amount"] = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                transferAmountEditText.text.toString()
            )

            DataLoader.postRequest(
                TransfersProgressBar,
                ApiMethods.TRANSFER,
                parameters,
                object : FutureCallBack<String> {
                    override fun onSuccess(result: String) {
                        val res = Gson().fromJson(result, TransferModel::class.java)

                        if (res.transactionStatus == "success") {
                            showSuccessMessage(
                                this@TransfersActivity,
                                App.instance.getContext().getString(R.string.success),
                                App.instance.getContext()
                                    .getString(R.string.transfer_complete_successfully)
                            )
                        } else {
                            showErrorMessage(this@TransfersActivity, "error", "Transaction failed")
                        }
                    }

                    override fun onFailure(title: String, errorMessage: String) {
                        showErrorMessage(this@TransfersActivity, title, errorMessage)
                    }
                }
            )
        }

        backButton.setOnClickListener {
            finish()
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        }
    }

}